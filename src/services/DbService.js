import jsonData from '../assets/json/db.json'

const loadData = () => {
  return JSON.parse(JSON.stringify(jsonData))
}

const getAllHookLeads = () => {
  let data = loadData()

  for(let i = 0; i < data.length; i++){
    console.log('Data index: ' + i + ', ' + data[i])
  }

  return data
}

export default getAllHookLeads
// export default DbService {loadData, getAllHookLeads}