//@flow
import React, { Component } from 'react';
import './App.css';
import { withStyles } from '@material-ui/core/styles';
import Home from './components/Home/Home';
import ErrorPage from './components/ErrorPage/ErrorPage';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

class App extends Component {

  render() {

    return (
      <Router>
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route component={ErrorPage} />
        </Switch>
      </Router>
    );
  }
}

export default App;
