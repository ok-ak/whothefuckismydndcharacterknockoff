//@flow
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import jsonData from '../../assets/json/db.json'

const loadData = () => {
  return JSON.parse(JSON.stringify(jsonData))
}

const getAllHookLeads = () => {
  let data = loadData()
  return data['hook_leads']
}

const getAllHookEnds = () => {
  let data = loadData()
  return data['hook_ends']
}

const getAllRaceAdjectives = () => {
  let data = loadData()
  return data['race_adjectives']
}

const getAllRaces = () => {
  let data = loadData()
  return data['races']
}

const getAllClasses = () => {
  let data = loadData()
  return data['classes']
}

const getAllBackgroundTransitions = () => {
  let data = loadData()
  return data['background_transitions']
}

const getAllBackgrounds = (characterClass) => {
  let data = loadData()
  return data['backgrounds'][characterClass]
}

const getAllBackgroundFlavors = () => {
  let data = loadData()
  return data['background_flavors']
}

const getAllLocationTransitionLeads = () => {
  let data = loadData()
  return data['location_transition_leads']
}

const getAllLocationTransitionEnds = () => {
  let data = loadData()
  return data['location_transition_ends']
}

const getAllLocationAdjectives = () => {
  let data = loadData()
  return data['location_adjectives']
}

const getAllLocations = () => {
  let data = loadData()
  return data['locations']
}

const getAllButtons = () => {
  let data = loadData()
  return data['buttons']
}

let getRandomEntry = (data) => {
  let arrayLength = data.length
  let randomIndex = Math.floor(Math.random() * (arrayLength -1 + 1))
  return data[randomIndex].toUpperCase()
}

let currentClass = null
const getClass = () => {
  currentClass = [...getRandomEntry(getAllClasses())].join('')
  return currentClass
}

const Body = (props: PropTypes) => {

  return(
    <div className="Body">
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Grid item xs={8}>
          <Paper className="p-l-16 p-r-16 p-t-16 p-b-16" elevation={1}>
            <Typography variant="title">
              {getRandomEntry(getAllHookLeads()) + ', ' + getRandomEntry(getAllHookEnds())}
            </Typography>
            <Typography variant="subheading">
              {
                getRandomEntry(getAllRaceAdjectives()) + ' ' + 
                getRandomEntry(getAllRaces()) + ' ' + getClass() + ' WHO ' +
                getRandomEntry(getAllBackgroundTransitions()) + ' ' + getRandomEntry(getAllBackgrounds(currentClass)) + ' AND '
                
              }
            </Typography>
            <Typography variant="subheading">
              {
                getRandomEntry(getAllBackgroundFlavors())
              }
              </Typography>
              <Typography variant="subheading">
              {
                ' AND ALSO ' + 
                getRandomEntry(getAllLocationTransitionLeads()) + ' FROM ' + getRandomEntry(getAllLocationTransitionEnds()) + ' ' + 
                getRandomEntry(getAllLocationAdjectives()) + ' ' + getRandomEntry(getAllLocations())
              }
            </Typography>
            <Button onClick={props.changed} button={() => getRandomEntry(getAllButtons())}>
              {getRandomEntry(getAllButtons())}
            </Button>
          </Paper>
        </Grid>
      </Grid>
    </div>
  )
}

export default Body;