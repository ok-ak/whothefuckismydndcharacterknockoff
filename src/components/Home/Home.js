//@flow
import React, { Component } from 'react';
import Body from '../Body/Body';
import Footer from '../Footer/Footer';

class Home extends Component {

  state = {
    button: ''
  }

  bodyChangeHandler = (event: Object) => {
    this.setState({button: event.target.button})
  }

  render() {

    return (
      <div className="Home">
          <div className="m-t-16 m-b-16">
            <Body
              button={this.state.button} 
              changed={(event) => this.bodyChangeHandler(event)}
              />
          </div>
          <div className="m-t-16 m-b-16">
            <Footer />
          </div>
      </div>
    );
  }
}

export default Home