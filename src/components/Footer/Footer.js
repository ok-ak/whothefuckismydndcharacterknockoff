//@flow
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';

const Footer = (props: PropTypes) => {

  return(
    <div className="Footer">
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Grid
          item xs={8}
        >
          <Paper className="p-l-16 p-r-16 p-t-16 p-b-16" elevation={0}>
            <Typography
              variant="subheading"
              align="right"
            >
              {
                'This knockoff was brought to you by Alex '
              }
              <a href="https://gitlab.com/ok-ak/whothefuckismydndcharacterknockoff">GitLab Repo</a>
            </Typography>
            <Typography
              variant="subheading"
              align="right"
            >
              {
                'The original inspiration for this knockoff was brought to you by Ryan '
              }
              <a href="http://whothefuckismydndcharacter.com">Clickity-Click</a>
            </Typography>
          </Paper>
        </Grid>
      </Grid>
    </div>
  )
}

export default Footer;