//@flow
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import style from './ErrorPage.css'
import {
  Link
} from 'react-router-dom';

const ErrorPage = (props: PropTypes) => {

  return(
    <div className="ErrorPage" style={style}>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Grid
          item xs={8}
        >
          <Paper className="p-l-16 p-r-16 p-t-16 p-b-16" elevation={1}>
            <Typography
              variant="title"
              align="center"
            >
              {
                'Yay you found where the easter egg was!'
              }
            </Typography>
            <br/>
            <Typography
              variant="heading"
              align="center"
            >
              {
                'Unfortunately James Donovan Halliday already gave it to Parzival. Better luck next time!'
              }
            </Typography>
            <br/>
              <Link to="/" className="myLink">
                <Button>
                  {'OK take me home now'}
                </Button>
              </Link>
          </Paper>
        </Grid>
      </Grid>
    </div>
  )
}

export default ErrorPage;