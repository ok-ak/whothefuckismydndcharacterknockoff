# whothefuckismydndcharacterknockoff

Find this gem in action at http://whothefuckismydndcharacterknockoff.surge.sh/

Just know that its the ebay equivalent of http://whothefuckismydndcharacter.com/

So what its a knockoff. You hate then you love it. Then you hate it some more as you buy more of it.

You're welcome.

Actually this this is a pet project to try out both React and Surge.sh on something fun. Maaad props to Ryan and Justin who are the presumable originators of the hilarious site and general theme linked above.

## Lessons Learned
1. Surge.sh is sweet but you can only deploy something with index.html at the root. No worries because...
  1. `cd <your tremendous react project root where you've been typing 'npm start' all this time for dev tinkering>`
  2. `npm run build` This will generate an optimized production build in a folder labeled build at your project root
2. `cd <your tremendous react project root>/build`
3. `surge` Surge deploys the folder you currently in. This means /build folder is the root from Surge's perspective and it has the index.html file it so desperately needs.
4. Answer the prompts to login
5. When surge prompts for a domain, edit that to your hearts content per the pricing criteria you happen to have.
6. Visit your tremendous deployed react project at the domain you specified in the terminal and tell all your friends.
